<?php

declare(strict_types=1);

namespace NORA\Oauth;

interface AccessTokenRefreshInterface
{
    public function refresh(AccessTokenInterface $token, ?bool &$refreshed = null): AccessTokenInterface;
    public function isAccessTokenExpired(AccessTokenInterface $token): bool;
}
