<?php

namespace NORA\Oauth\Infra;

use NORA\Oauth\AccessTokenInterface;
use NORA\Oauth\AccessTokenRefreshInterface;
use NORA\Oauth\AccessTokenRepoInterface as OauthAccessTokenRepoInterface;
use NORA\Storage\Kvs\KvsStorageInterface;

/**
 * @psalm-suppress UnusedClass
 */
final class AccessTokenRepo implements OauthAccessTokenRepoInterface
{
    public function __construct(private KvsStorageInterface $kvs)
    {
    }

    public function save(AccessTokenInterface $token, string $name = "default"): void
    {
        $this->kvs->store($name, serialize($token));
    }

    public function get(string $name = "default"): AccessTokenInterface
    {
        if ($this->kvs->has($name)) {
            $token = unserialize($this->kvs->get($name));
            assert($token instanceof AccessTokenInterface);
            return $token;
        }
        throw new \RuntimeException("{$name} is not stored");
    }

    public function has(string $name = "default"): bool
    {
        return $this->kvs->has($name);
    }

    public function refresh(
        AccessTokenRefreshInterface $sdk,
        string $name = 'default'
    ): AccessTokenInterface
    {
        $token = $this->get($name);
        if (!$sdk->isAccessTokenExpired($token)) {
            return $token;
        }
        $newToken = $sdk->refresh($token);
        $this->save($newToken, $name);
        return $token;
    }
}
