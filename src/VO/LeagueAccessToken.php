<?php

declare(strict_types=1);

namespace NORA\Oauth\VO;

use League\OAuth2\Client\Token\AccessToken;
use NORA\Oauth\AccessTokenInterface;

final class LeagueAccessToken implements AccessTokenInterface
{
    public function __construct(private AccessToken $token)
    {
    }

    public function getToken(): string
    {
        return $this->token->getToken();
    }

    public static function fromFile(string $path): AccessTokenInterface
    {
        $text = file_get_contents($path);
        return self::fromString($text);
    }

    public static function fromString(string $text): AccessTokenInterface
    {
        return unserialize($text);
    }

    public function __toString(): string
    {
        return serialize($this);
    }

    public function getLeagueAccessToken(): AccessToken
    {
        return $this->token;
    }

    public function hasExpired() : bool
    {
        return $this->token->hasExpired();
    }

    public function getRefreshToken() : string
    {
        return $this->token->getRefreshToken();
    }
}
