<?php

namespace NORA\Oauth;

/**
 * @psalm-suppress UnusedClass
 */
interface AccessTokenRepoInterface
{
    public function save(AccessTokenInterface $token, string $name = "default"): void;
    public function get(string $name = "default"): AccessTokenInterface;
    public function has(string $name = "default"): bool;
    public function refresh(AccessTokenRefreshInterface $sdk, string $name = 'default'): AccessTokenInterface;
}
