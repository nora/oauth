<?php

declare(strict_types=1);

namespace NORA\Oauth\Usecase;

final class RunCallbackServer
{
    /**
     * @return array{code: string, state: string}
     */
    public function __invoke(int $port = 8999): array
    {
        $server = stream_socket_server(
            "tcp://0.0.0.0:{$port}",
            $errno,
            $errmsg,
            STREAM_SERVER_LISTEN | STREAM_SERVER_BIND
        );

        assert(is_resource($server));

        $query = [];
        while ($conn = stream_socket_accept($server)) {
            echo "\r\nSocket accepted\r\n";
            while ($line = fgets($conn, 1024)) {
                if ($line === "\r\n") {
                    break;
                }
                if (preg_match('/GET (.+) HTTP/', $line, $m)) {
                    $parts = parse_url($m[1]);
                    if (!isset($parts['query'])) {
                        continue;
                    }
                    parse_str($parts['query'], $query);
                    break 2;
                }
            }
            fwrite($conn, "HTTP/1.1 201 OK\r\n");
            fwrite($conn, "\r\n");
            fclose($conn);
        }
        fclose($server);

        assert(isset($query["code"]));
        assert(isset($query["state"]));
        assert(is_string($query["code"]));
        assert(is_string($query["state"]));

        return [
            'code' => $query['code'],
            'state' => $query['state']
        ];
    }
}
