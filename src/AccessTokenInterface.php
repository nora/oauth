<?php

declare(strict_types=1);

namespace NORA\Oauth;

interface AccessTokenInterface
{
    public function __toString(): string;
    public static function fromFile(string $path): self;
    public static function fromString(string $token): self;
    public function getToken(): string;
    public function hasExpired(): bool;
    public function getRefreshToken(): string;
}
