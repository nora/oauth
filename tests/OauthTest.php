<?php

declare(strict_types=1);

namespace NORA\Oauth;

use PHPUnit\Framework\TestCase;

class OauthTest extends TestCase
{
    protected Oauth $oauth;

    protected function setUp(): void
    {
        $this->oauth = new Oauth();
    }

    public function testIsInstanceOfOauth(): void
    {
        $actual = $this->oauth;
        $this->assertInstanceOf(Oauth::class, $actual);
    }
}
